#!/usr/bin/env bash
date=1967
for (( i = 1; i < 40; i++ )); do
  mkdir $date
  for (( u = 1; u < 5; u++ )); do
    echo $date  $compt  $u
    wget -P $date 'https://s3-us-west-2.amazonaws.com/visiblelanguage/pdf/V'$i'N'$u'_'$date'_E.pdf'
  done
  date=$((date+1))
done
