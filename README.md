# Visible Langage Aspirator

It's a short script for download ~ 40 years of [Visible-Language](https://en.wikipedia.org/wiki/Visible_Language) (pdf).

## Requirement
  
  [wget](https://www.gnu.org/software/wget/)

## Execute script


`$ cd  Visible-Langage-Aspirator/`


`$ bash V-L-A.sh`

--------------------
A.Gelgon
